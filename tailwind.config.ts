import type { Config } from 'tailwindcss';
import { nextui } from '@nextui-org/react';

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    './node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
    },
    backgroundImage: {
      'cta-pattern':
        "url('https://res.cloudinary.com/dysce4sh2/image/upload/v1701066325/b5y7hrle46q7dqto00pw.png')",
      'pattern-2':
        "url('https://res.cloudinary.com/dysce4sh2/image/upload/v1701085575/soaueysybx9vs8es0wqz.png')",
      'pattern-3':
        "url('https://res.cloudinary.com/dysce4sh2/image/upload/v1701087378/bmfbq6y3f6kvvnyv7flk.png')",
    },
  },
  darkMode: 'class',
  plugins: [nextui()],
};
export default config;
