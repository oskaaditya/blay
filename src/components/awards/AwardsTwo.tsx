import Image from 'next/image';
import React from 'react';

const AwardsTwo = () => {
  return (
    <Image
      src={
        'https://res.cloudinary.com/dysce4sh2/image/upload/v1701056683/cfgxvai1e9i47sjq1xvp.png'
      }
      width={82}
      height={48}
      quality={100}
      alt='awards-1'
      className='h-auto w-full max-w-[82px]'
    />
  );
};

export default AwardsTwo;
