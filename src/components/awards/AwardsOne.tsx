import Image from 'next/image';
import React from 'react';

const AwardsOne = () => {
  return (
    <Image
      src={
        'https://res.cloudinary.com/dysce4sh2/image/upload/v1701056683/ohwetrgiv9xvlou3secd.png'
      }
      width={34}
      height={48}
      alt='awards-1'
      className='h-auto w-full max-w-[34px]'
    />
  );
};

export default AwardsOne;
