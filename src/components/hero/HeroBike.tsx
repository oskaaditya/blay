'use client';

import { BikeHeroList } from '@/app/constants';
import { Button } from '@nextui-org/react';
import Image from 'next/image';
import React, { useState } from 'react';
import { Appear, Zoom } from '../layouts/Animate';

const HeroBike = () => {
  const [active, setActive] = useState<number>(0);

  const handleActive = (num: number) => {
    setActive(num);
  };

  return (
    <>
      <div className='mt-10 flex w-fit items-center rounded-full bg-[#EFEFEF] p-4 shadow-md md:mt-[139px]'>
        <Appear delayTime={0.8}>
          {BikeHeroList.map((item, index) => (
            <Button
              key={index}
              className={`shadow-inner-md mr-[18px] h-10 w-10 min-w-0 rounded-full shadow-md ${
                active === index ? 'border-2 border-black' : ''
              }`}
              style={{ backgroundColor: item.color }}
              onClick={() => handleActive(index)}
            />
          ))}
        </Appear>
        <Zoom>
          <Image
            src={
              'https://res.cloudinary.com/dysce4sh2/image/upload/v1701161319/bquoldalp8uuvo0qg0qo.png'
            }
            width={1363}
            height={800}
            quality={100}
            alt='bike-hero'
            className='absolute bottom-0 right-10 h-auto w-full max-w-[800px] sm:right-0 lg:max-w-[950px] xl:max-w-[1363px]'
          />
        </Zoom>
      </div>
    </>
  );
};

export default HeroBike;
