import { Card, CardBody, CardHeader } from '@nextui-org/react';
import Image from 'next/image';
import React, { FC } from 'react';

interface BikeCardProps {
  name: string;
  description: string;
  url: string;
  background: string;
}

const BikeCard: FC<BikeCardProps> = ({
  name,
  description,
  url,
  background,
}) => {
  return (
    <Card
      className={`relative overflow-hidden rounded-lg shadow-none`}
      style={{ backgroundColor: background }}
    >
      <CardHeader className='flex-col items-end p-6'>
        <p className='font-["Inter"]  text-2xl font-medium text-black'>
          {name}
        </p>
        <p className='text-md text-[#363D4F]'>{description}</p>
      </CardHeader>
      <CardBody className='-mb-[15%] -ml-[15%] mt-10 p-0'>
        <Image
          src={url}
          width={659}
          height={276}
          quality={100}
          alt='bike'
          className=' h-auto w-full max-w-[659px]'
        />
      </CardBody>
    </Card>
  );
};

export default BikeCard;
