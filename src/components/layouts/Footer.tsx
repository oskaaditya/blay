import React from 'react';
import AwardsOne from '../awards/AwardsOne';
import AwardsTwo from '../awards/AwardsTwo';
import { LinkMenu, SocialMediaLink } from '@/app/constants';
import Link from 'next/link';
import { Button, Input } from '@nextui-org/react';
import { Copyright, Linkedin } from 'lucide-react';

const Footer = () => {
  return (
    <footer className='bg-[#212121] px-6 pt-[104px]'>
      <div className='container mx-auto'>
        <div className='footer-top flex flex-wrap justify-between pb-20 xl:flex-nowrap'>
          <div className='footer-left'>
            <h1 className='text-2xl font-bold text-white'>Blay</h1>
            <p className='mt-[26px] tracking-[0.3px] text-white/60 md:max-w-[200px]'>
              Ride comfortably, safely, and discover the experience.
            </p>
            <div className='mt-10 flex items-center gap-6'>
              <AwardsOne />
              <AwardsTwo />
            </div>
          </div>
          <div className='footer-right mt-8 grid grid-cols-2 items-start gap-20 md:grid-cols-4 lg:mt-0  xl:gap-[120px]'>
            {LinkMenu.map((link, index) => (
              <div key={index}>
                <h3 className='mb-4 font-medium text-white'>
                  {link.header.name}
                </h3>
                <ul>
                  {link.submenu.map((item, index) => (
                    <li key={index} className='mb-3 text-white/60'>
                      <Link href={item.url}>{item.name}</Link>
                    </li>
                  ))}
                </ul>
              </div>
            ))}
          </div>
        </div>
        <div className='footer-center flex flex-col flex-wrap items-center justify-center border-y border-y-white/60 py-10 md:flex-row md:justify-between'>
          <p className='text-2xl font-medium text-white'>Stay in the loop</p>
          <Input
            className='mt-6 max-w-[352px] rounded-full bg-white lg:mt-0'
            radius='full'
            placeholder='Enter your email'
            endContent={
              <Button
                radius='full'
                className='-mr-2 bg-[#0071E3] px-6 py-2 text-sm font-medium text-white'
              >
                Subscribe
              </Button>
            }
          />
        </div>
        <div className='footer-bottom flex flex-wrap justify-center gap-4 py-12 md:justify-between'>
          <div>
            <p className='flex items-center tracking-[0.3px] text-white/60'>
              <Copyright className='mr-1' /> {new Date().getFullYear()} Blay.
              All right reserved
            </p>
          </div>
          <div className='flex flex-col items-center gap-6 sm:flex-row'>
            <Link href='#' className='tracking-[0.3px] text-white/60'>
              Privacy Policy
            </Link>
            <Link href='#' className='tracking-[0.3px] text-white/60'>
              Terms of Conditions
            </Link>
            <div className='flex items-center gap-4'>
              {SocialMediaLink.map((link, index) => (
                <Link
                  key={index}
                  href={link.url}
                  className='text-xl text-white/60'
                >
                  {React.createElement(link.icon)}
                </Link>
              ))}
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
