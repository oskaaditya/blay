'use client';

import {
  Button,
  Chip,
  Navbar,
  NavbarBrand,
  NavbarContent,
  NavbarItem,
  NavbarMenu,
  NavbarMenuItem,
  NavbarMenuToggle,
} from '@nextui-org/react';
import React, { useState } from 'react';
import Link from 'next/link';
import { Search, ShoppingBagIcon } from 'lucide-react';
import { NavbarMenuList } from '@/app/constants';

const NavbarComponent = () => {
  const [isChipVisible, setIsChipVisible] = useState(true);

  const handleCloseChip = () => {
    setIsChipVisible(false);
  };

  return (
    <>
      {isChipVisible && (
        <div className='p-2'>
          <Chip
            onClose={handleCloseChip}
            className='promo h-full w-full max-w-[full] rounded-sm bg-[#212121] py-[14px] text-center text-sm font-medium text-white transition-all'
          >
            Certified refurbished bikes from $5,200.{' '}
            <Link href={'#'} as={'span'} className=' underline'>
              {' '}
              Discover Bike
            </Link>
          </Chip>
        </div>
      )}
      <Navbar isBordered className=' bg-white px-6 lg:px-0'>
        <div className='container mx-auto flex items-center justify-between'>
          <NavbarContent justify='start'>
            <NavbarBrand>
              <h1 className='text-[32px] font-bold uppercase'>Blay</h1>
            </NavbarBrand>
          </NavbarContent>

          <NavbarContent className='hidden gap-10 lg:flex' justify='center'>
            {NavbarMenuList.map((item, index) => (
              <NavbarMenuItem key={`${item}-${index}`}>
                <Link className='w-full text-base font-medium' href={item.url}>
                  {item.name}
                </Link>
              </NavbarMenuItem>
            ))}
          </NavbarContent>

          <NavbarContent justify='end'>
            <NavbarItem className='hidden gap-4 lg:flex'>
              <Button
                isIconOnly
                className='bg-transparent text-[#060B13]'
                aria-label='Search'
              >
                <Search />
              </Button>
              <Button
                isIconOnly
                className='bg-transparent text-[#060B13]'
                aria-label='Shop'
              >
                <ShoppingBagIcon />
              </Button>
              <Button
                className=' bg-[#0071E3] px-8 py-4 font-medium text-white'
                radius='full'
              >
                Buy Now
              </Button>
            </NavbarItem>
          </NavbarContent>

          <NavbarMenu className='w-full items-center bg-white'>
            {NavbarMenuList.map((item, index) => (
              <NavbarMenuItem key={`${item}-${index}`}>
                <Link className='w-full text-base font-medium' href={item.url}>
                  {item.name}
                </Link>
              </NavbarMenuItem>
            ))}
            <NavbarItem className='hidden lg:flex'>
              <Button
                className='mt-10 bg-[#0071E3] px-8 py-4 font-medium text-white'
                radius='full'
              >
                Buy Now
              </Button>
            </NavbarItem>
          </NavbarMenu>

          <NavbarContent
            className='flex items-center gap-2 lg:hidden'
            justify='end'
          >
            <Button
              isIconOnly
              className='bg-transparent text-[#060B13]'
              aria-label='Search'
              size='sm'
            >
              <Search />
            </Button>
            <Button
              isIconOnly
              className='bg-transparent text-[#060B13]'
              aria-label='Shop'
              size='sm'
            >
              <ShoppingBagIcon />
            </Button>
            <Button
              className=' bg-[#0071E3] px-8 py-4 font-medium text-white'
              radius='full'
              size='sm'
            >
              Buy Now
            </Button>
            <NavbarMenuToggle />
          </NavbarContent>
        </div>
      </Navbar>
    </>
  );
};

export default NavbarComponent;
