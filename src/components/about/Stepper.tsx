'use client';
import { ContentAbout } from '@/app/constants';
import { Button, Progress } from '@nextui-org/react';
import React, { useState } from 'react';

const Stepper: React.FC = () => {
  const [activeStep, setActiveStep] = useState<number>(0);
  const [progress, setProgress] = useState(33);

  const handleStepClick = (step: number) => {
    setActiveStep(step);
    if (step === 0) {
      setProgress(33);
    } else if (step === 1) {
      setProgress(66);
    } else if (step === 2) {
      setProgress(100);
    }
  };

  return (
    <div>
      <Progress
        value={progress}
        size='sm'
        radius='none'
        classNames={{
          base: 'max-w-full',
          indicator: '!bg-black',
          value: 'text-foreground/60',
        }}
      />
      <div className='mt-[77px] grid grid-cols-1 items-start md:grid-cols-2'>
        <div>
          {ContentAbout.map((content, index) => (
            <div
              key={index}
              onClick={() => handleStepClick(index)}
              className='mb-[34px] flex cursor-pointer items-center gap-6 text-[32px] font-medium'
            >
              <Button
                className={`w-[40px] min-w-0 text-sm md:text-xl  ${
                  activeStep === index
                    ? 'rounded-full bg-black px-2 text-white'
                    : 'bg-transparent text-[#979FB4]'
                } `}
                onClick={() => handleStepClick(index)}
              >
                {index + 1}
              </Button>{' '}
              <p
                className={`${
                  activeStep === index ? '' : 'text-[#979FB4]'
                } text-md md:text-xl`}
              >
                {content.name}
              </p>
            </div>
          ))}
        </div>

        <div className='right-content'>
          <h2 className='mb-6 text-4xl font-medium md:text-5xl'>
            {ContentAbout[activeStep].title}
          </h2>
          <p className='mb-4 text-base md:text-lg'>
            {ContentAbout[activeStep].paragraph1}
          </p>
          <p className='mb-4 text-base md:text-lg'>
            {ContentAbout[activeStep].paragraph2}
          </p>
        </div>
      </div>
    </div>
  );
};

export default Stepper;
