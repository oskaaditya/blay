import { DescCardDetailsFourList } from '@/app/constants';
import Image from 'next/image';
import React, { FC } from 'react';

export interface CardDetailsFourProps {
  title: string;
  description: string;
}

const CardDetailsFour: FC<CardDetailsFourProps> = ({ title, description }) => {
  return (
    <div className='w-full overflow-hidden rounded-lg bg-[#212121]'>
      {DescCardDetailsFourList.map((item, index) => (
        <div
          key={index}
          className='flex flex-col items-start justify-start gap-3 p-10 pb-0'
        >
          <h2 className='text-lg font-medium text-white lg:text-2xl'>
            {item.title}
          </h2>
          <p className='text-md max-w-[422px] font-normal text-white/60'>
            {item.description}
          </p>
        </div>
      ))}
      <div className='flex justify-center'>
        <Image
          src={
            'https://res.cloudinary.com/dysce4sh2/image/upload/v1701089608/xunz3ciw7rp4mgohh6j3.png'
          }
          width={432}
          height={423}
          quality={100}
          className='h-auto w-full max-w-[250px] xl:-mt-10 xl:max-w-[432px]'
          alt='wheels'
        />
      </div>
    </div>
  );
};

export default CardDetailsFour;
