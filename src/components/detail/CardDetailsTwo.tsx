import Image from 'next/image';
import React, { FC } from 'react';

interface CardDetailTwoProps {
  title: string;
  description: string;
}

const CardDetailTwo: FC<CardDetailTwoProps> = ({ title, description }) => {
  return (
    <div className='w-full overflow-hidden rounded-lg bg-[#212121] md:max-h-[720px] xl:h-[720px]'>
      <div className='h-[180px] bg-pattern-2 bg-cover bg-center lg:h-[286px]'>
        <div className='relative  z-0 flex justify-center '>
          <Image
            src={
              'https://res.cloudinary.com/dysce4sh2/image/upload/v1701085733/wofmt1ko5gu8lzxaelz3.png'
            }
            width={1880}
            height={720}
            alt='bike-phone'
            quality={100}
            className='h-auto w-full max-w-[700px] md:mt-0 md:max-w-[1024px] xl:max-w-[1880px]'
          />
          <Image
            src={
              'https://res.cloudinary.com/dysce4sh2/image/upload/v1701086370/acky0flgq1zmg4p6izgj.png'
            }
            width={48}
            height={48}
            alt='flower'
            quality={100}
            className='absolute top-[45%] max-h-[24px] max-w-[24px] md:top-[50%] md:max-h-[48px] md:max-w-[48px]'
          />
        </div>
      </div>
      <div className='z-10 mt-12 flex h-full flex-col items-start justify-center gap-3 pb-[5%] pl-[5%] md:pb-[2%]  xl:mt-0 xl:pb-[5%]'>
        <h2 className='text-lg font-medium text-white lg:text-2xl'>{title}</h2>
        <p className='text-md  max-w-[260px] text-start font-normal text-white/60 lg:text-base xl:max-w-[422px]'>
          {description}
        </p>
      </div>
    </div>
  );
};

export default CardDetailTwo;
