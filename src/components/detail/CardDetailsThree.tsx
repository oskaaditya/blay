import Image from 'next/image';
import React from 'react';

const CardDetailThree = () => {
  return (
    <div>
      <div className='relative h-full w-full overflow-hidden rounded-lg bg-[#212121]'>
        <div className='flex justify-center pt-[72px]'>
          <Image
            src={
              'https://res.cloudinary.com/dysce4sh2/image/upload/v1701087378/u9hfmot69ibkkoonmwpr.png'
            }
            height={648}
            width={597}
            quality={100}
            alt='bike-three'
            className='-mb-[186px] h-auto w-full max-w-[597px]'
          />
        </div>
        <div className='h-[186px] bg-pattern-3 bg-cover bg-center'></div>
      </div>
    </div>
  );
};

export default CardDetailThree;
