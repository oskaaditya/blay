import Image from 'next/image';
import React, { FC } from 'react';

interface CardDetailsOneProps {
  title: string;
  description: string;
}

const CardDetailsOne: FC<CardDetailsOneProps> = ({ title, description }) => {
  return (
    <div className='w-full overflow-hidden rounded-lg bg-[#212121]'>
      <div className='flex flex-col items-center justify-center gap-3 px-6 pt-20'>
        <h2 className='text-2xl font-medium text-white'>{title}</h2>
        <p className='max-w-[422px] text-center font-normal text-white/60'>
          {description}
        </p>
      </div>
      <div>
        <Image
          src={
            'https://res.cloudinary.com/dysce4sh2/image/upload/v1701080484/efjzzxre6kuhsijcsck9.png'
          }
          width={620}
          height={540}
          quality={100}
          className='h-auto w-full lg:-mt-28 '
          alt='wheels'
        />
      </div>
    </div>
  );
};

export default CardDetailsOne;
