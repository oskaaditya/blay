import { CardDetailsFourProps } from '@/components/detail/CardDetailsFour';
import { StepContent } from '@/interfaces/about';
import { BikeProps, SpecificationListProps } from '@/interfaces/bike';
import { BikeHeroProps } from '@/interfaces/hero';
import {
  LinkMenuProps,
  SocialMediaLinkProps,
  LinkProps,
} from '@/interfaces/link';
import { FaLinkedinIn, FaTwitter, FaFacebook } from 'react-icons/fa';

export const NavbarMenuList: Array<LinkProps> = [
  {
    name: 'Commuter',
    url: '#',
  },
  {
    name: 'Accessories',
    url: '#',
  },
  {
    name: 'Services',
    url: '#',
  },
  {
    name: 'About',
    url: '#',
  },
];

export const LinkMenu: Array<LinkMenuProps> = [
  {
    header: {
      name: 'Products',
      url: '#products',
    },
    submenu: [
      {
        name: 'Ordinal',
        url: '#',
      },
      {
        name: 'Ordinal ST',
        url: '#',
      },
      {
        name: 'Turbo',
        url: '#',
      },
      {
        name: 'Diverge Lord',
        url: '#',
      },
      {
        name: 'Tempest ST',
        url: '#',
      },
    ],
  },
  {
    header: {
      name: 'Services',
      url: '#services',
    },
    submenu: [
      {
        name: 'Blay Care',
        url: '#',
      },
      {
        name: 'Warranty',
        url: '#',
      },
      {
        name: 'Leasing',
        url: '#',
      },
      {
        name: 'Business',
        url: '#',
      },
      {
        name: 'Blay Insurance',
        url: '#',
      },
    ],
  },
  {
    header: {
      name: 'About',
      url: '#about',
    },
    submenu: [
      {
        name: 'Blog',
        url: '#',
      },
      {
        name: 'Reviews',
        url: '#',
      },
      {
        name: 'Stores',
        url: '#',
      },
      {
        name: 'Careers',
        url: '#',
      },
      {
        name: 'The Stories',
        url: '#',
      },
    ],
  },
  {
    header: {
      name: 'Help',
      url: '#help',
    },
    submenu: [
      {
        name: 'Support',
        url: '#',
      },
      {
        name: 'Contact',
        url: '#',
      },
      {
        name: 'Delivery',
        url: '#',
      },
      {
        name: 'Return',
        url: '#',
      },
    ],
  },
];

export const SocialMediaLink: Array<SocialMediaLinkProps> = [
  {
    icon: FaLinkedinIn,
    url: '#',
  },
  {
    icon: FaTwitter,
    url: '#',
  },
  {
    icon: FaFacebook,
    url: '#',
  },
];

export const BikeCardList: Array<BikeProps> = [
  {
    name: 'Blacky',
    url: 'https://res.cloudinary.com/dysce4sh2/image/upload/v1701069820/btqmrgsqhsii39ypmnf6.png',
    background: '#FFFFFF',
    description: 'Available in 10 days',
  },
  {
    name: 'Creamy',
    url: 'https://res.cloudinary.com/dysce4sh2/image/upload/v1701069083/lmbmanxe79vkhzhjvaod.png',
    background: '#F5EEE2',
    description: 'Available in 10 days',
  },
  {
    name: 'Greeny',
    url: 'https://res.cloudinary.com/dysce4sh2/image/upload/v1701069915/gn7nu9v6oumxgemy1zuo.png',
    background: '#F5EEE2',
    description: 'Available in 10 days',
  },
  {
    name: 'Purpley',
    url: 'https://res.cloudinary.com/dysce4sh2/image/upload/v1701076717/a5fe8nxmvgphhpplv1pn.png',
    background: '#E5E7FB',
    description: 'Available in 10 days',
  },
];

export const SpecificationList: Array<SpecificationListProps> = [
  {
    name: 'Frameset',
    part: [
      {
        name: 'Frame',
        description: 'Ordinal Carbon',
      },
      {
        name: 'Fork',
        description: 'Ordinal Carbon, 1-1/8" to 1.5" steerer',
      },
      {
        name: 'Headset',
        description: 'Integrated, Carbon, 1-1/8" - 1-1/2"',
      },
    ],
  },
  {
    name: 'Wheels',
    part: [
      {
        name: 'Rims',
        description: 'Ordinal Light i23 TCS, 700c, 28h, tubeless',
      },
      {
        name: 'Spokes',
        description: 'Stainless Steel, 14g',
      },
      {
        name: 'Wheel Size',
        description: '700c',
      },
      {
        name: 'Tires',
        description: 'Ordinal Riddler TCS Light, 700x45c, tubeless',
      },
    ],
  },
];
export const DescCardDetailsFourList: Array<CardDetailsFourProps> = [
  {
    title: 'Versatile',
    description:
      'The saddle provides comfort to nearly every rider measured in our database. You can customize your ride for best experiences.',
  },
  {
    title: 'Future Saddle',
    description:
      'We`re designing the saddle of the future, where Product Manager Rifqy Aulia explains that commuter bikes should be made stiff and how they can smooth out all the bumps experience.',
  },
];

export const ContentAbout: Array<StepContent> = [
  {
    name: 'Capabilty',
    title: 'Turning seems impossible into possible in Ordinal’s',
    paragraph1:
      'Get up to 120 miles of range, progressive geometry, and tire size options that suit anything from fast road to rugged gravel. Load up racks for the long haul, and get low and in control with a spec’d dropper post.',
    paragraph2:
      'Once we set the bar of capability and handling, we boosted the experience with the unbelievably natural power of our advanced, fully integrated, and whisper-quiet.',
  },
  {
    name: 'Compliance',
    title: 'Where does it come from?',
    paragraph1:
      'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. ',
    paragraph2:
      'Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. ',
  },
  {
    name: 'Amplification',
    title: 'Contrary to popular belief',
    paragraph1:
      'Get up to 120 miles of range, progressive geometry, and tire size options that suit anything from fast road to rugged gravel. Load up racks for the long haul, and get low and in control',
    paragraph2:
      'Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage',
  },
];

export const BikeHeroList: Array<BikeHeroProps> = [
  {
    color: '#343434',
    url: 'https://res.cloudinary.com/dysce4sh2/image/upload/v1701161319/bquoldalp8uuvo0qg0qo.png',
  },
  {
    color: '#768277',
    url: 'https://res.cloudinary.com/dysce4sh2/image/upload/v1701161319/bquoldalp8uuvo0qg0qo.png',
  },
  {
    color: '#BEBAB1',
    url: 'https://res.cloudinary.com/dysce4sh2/image/upload/v1701161319/bquoldalp8uuvo0qg0qo.png',
  },
  {
    color: '#EAEAEA',
    url: 'https://res.cloudinary.com/dysce4sh2/image/upload/v1701161319/bquoldalp8uuvo0qg0qo.png',
  },
];
