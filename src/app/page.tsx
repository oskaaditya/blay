import BikeCard from '@/components/specifications/BikeCard';
import { Button } from '@nextui-org/react';
import Image from 'next/image';
import { BikeCardList, SpecificationList } from './constants';
import { Plus } from 'lucide-react';
import CardDetailsOne from '@/components/detail/CardDetailsOne';
import CardDetailsTwo from '@/components/detail/CardDetailsTwo';
import CardDetailThree from '@/components/detail/CardDetailsThree';
import CardDetailsFour from '@/components/detail/CardDetailsFour';
import Stepper from '@/components/about/Stepper';
import HeroBike from '@/components/hero/HeroBike';
import { Appear, Zoom } from '@/components/layouts/Animate';

export default function Home() {
  return (
    <main>
      <section className='relative px-6 pt-16 md:pt-[169px] '>
        <div className='container mx-auto'>
          <div className='flex items-center'>
            <div>
              <Appear delayTime={0.2}>
                <p className='text-xl font-medium'>Modern Commuter</p>
              </Appear>
              <Appear delayTime={0.4}>
                <h1 className='mt-4 max-w-[515px] text-7xl font-medium leading-none md:text-[120px]'>
                  Ordinal Scale
                </h1>
              </Appear>
              <Appear delayTime={0.6}>
                <div className='mt-6 flex items-center gap-4'>
                  <Button
                    className=' bg-[#0071E3] px-8 py-4 font-medium text-white'
                    radius='full'
                  >
                    Buy Now
                  </Button>
                  <p>From $5,200</p>
                </div>
              </Appear>
              <HeroBike />
            </div>
          </div>
        </div>
        <div className='-mx-6 h-[286px] bg-cta-pattern bg-cover bg-center lg:h-[350px]' />
      </section>
      <section className='px-6 py-[104px]'>
        <div className='container mx-auto'>
          <div className='flex flex-wrap items-end justify-between gap-6'>
            <Appear delayTime={0.5}>
              <h1 className='text-4xl font-medium md:text-5xl lg:max-w-md'>
                We bring the future of Commuter Bike
              </h1>
            </Appear>
            <Appear delayTime={0.7}>
              <p className='text-[#363D4F] lg:max-w-xl'>
                Modern in style, available in an adaptable commuter bike format
                for daily needs, Ordinal features geometry designed for daily
                comfort. Made of carbon material, with 700C x 38C tires and
                equipped with excellent features.
              </p>
            </Appear>
          </div>
          <Zoom delayTime={0.8}>
            <Image
              src={
                'https://res.cloudinary.com/dysce4sh2/image/upload/v1701092918/fkr4zcjnj0uzzggztnvh.png'
              }
              quality={100}
              width={1280}
              height={760}
              alt='phone'
              className='my-[50px] max-h-[760px] md:h-auto md:w-full'
            />
          </Zoom>
          <Appear delayTime={0.6}>
            <Stepper />
          </Appear>
        </div>
      </section>
      <section className='bg-[#060B13] px-6 py-[104px]'>
        <div className='container mx-auto'>
          <div className='mb-9 grid grid-cols-1 items-center gap-6 lg:grid-cols-2 lg:gap-0'>
            <div>
              <Appear delayTime={0.2}>
                <h1 className='text-4xl font-medium tracking-tight text-white md:max-w-[515px] md:text-5xl'>
                  Comfortable ride with Ordinal&apos;s top-quality
                </h1>
              </Appear>
              <Appear delayTime={0.4}>
                <p className='mt-6 max-w-[515px] text-base text-white/60 md:text-lg'>
                  Modern in style, available in an adaptable commuter bike
                  format for daily needs, Ordinal features geometry designed for
                  daily comfort.
                </p>
              </Appear>
              <Appear delayTime={0.6}>
                <Button
                  className='mt-10 bg-[#0071E3] px-8 py-4 font-medium text-white'
                  radius='full'
                >
                  Buy Now
                </Button>
              </Appear>
            </div>
            <Zoom delayTime={1}>
              <CardDetailsOne
                title='Ordinal Wheelset'
                description='The perfect wheel for a daily ride, Ordinal’s 32mm-deep section rim feels like riding on a knife-edge.'
              />
            </Zoom>
          </div>
          <Zoom delayTime={0.5}>
            <CardDetailsTwo
              title='Unleash Your Dream Ride'
              description='With advanced performance tracking, seamless partner app integration, and Ordinal’s e-bike management, the all-new specialized app takes cycling and connectivity to the next level.'
            />
          </Zoom>
          <div className='mt-10 grid grid-cols-1 gap-10 lg:grid-cols-2'>
            <Zoom delayTime={0.5}>
              <CardDetailThree />
            </Zoom>
            <Zoom delayTime={0.7}>
              <CardDetailsFour title='as' description='as' />
            </Zoom>
          </div>
        </div>
      </section>
      <section className='px-6 py-[104px]'>
        <div className='container mx-auto'>
          <div className='flex flex-wrap items-end justify-between gap-6'>
            <Appear delayTime={0.5}>
              <h1 className='text-4xl font-medium md:text-5xl lg:max-w-md'>
                Dream technical specifications
              </h1>
            </Appear>
            <Appear delayTime={0.7}>
              <p className='text-base text-[#363D4F] md:text-lg lg:max-w-xl'>
                Ordinal commuter bikes are also beneficial for your health and
                well-being due to their ergonomic design. Comfortable riding can
                provide a low-impact cardiovascular workout and help reduce
                stress.
              </p>
            </Appear>
          </div>
          <div className='mt-16 grid gap-10 sm:grid-cols-2'>
            {BikeCardList.map((item, index) => (
              <Zoom key={index} delayTime={0.3}>
                <BikeCard
                  name={item.name}
                  description={item.description}
                  background={item.background}
                  url={item.url}
                />
              </Zoom>
            ))}
          </div>
          {SpecificationList.map((spec, index) => (
            <Appear key={index}>
              <div className='items start mt-16 grid grid-cols-3 justify-between'>
                <p className='text-xl font-medium'>{spec.name}</p>
                <div className='flex  flex-col md:gap-16'>
                  {spec.part.map((item, index) => (
                    <p
                      key={index}
                      className='min-h-[100px] text-base md:min-h-0 md:text-xl'
                    >
                      {item.name}
                    </p>
                  ))}
                </div>
                <div className='flex min-h-fit flex-col md:gap-16'>
                  {spec.part.map((item, index) => (
                    <p
                      key={index}
                      className='text-md min-h-[100px] md:min-h-0 md:text-xl'
                    >
                      {item.description}
                    </p>
                  ))}
                </div>
              </div>
            </Appear>
          ))}
          <div className='flex justify-center'>
            <Appear delayTime={0.5}>
              <Button
                className='mt-16 rounded-full bg-[#060B13] py-6 pl-6 pr-2 text-white'
                endContent={
                  <div className='ml-6 transform rounded-full bg-[#0071E3] p-1 transition-transform hover:scale-110'>
                    <Plus />
                  </div>
                }
              >
                Show Specification
              </Button>
            </Appear>
          </div>
        </div>
      </section>
      <section className='overflow-hidden pt-[104px]'>
        <div className='container mx-auto'>
          <div className='mb-[90px] flex flex-col items-center justify-center'>
            <Appear delayTime={0.4}>
              <p className='mb-4 text-xl font-medium md:text-2xl'>
                Ordinal Scale
              </p>
            </Appear>
            <Appear delayTime={0.6}>
              <h1 className='text-5xl font-medium lg:text-[120px]'>It’s You</h1>
            </Appear>
            <Appear delayTime={0.8}>
              <h1 className='text-5xl font-medium lg:text-[120px]'>
                Only Faster
              </h1>
            </Appear>
            <Appear delayTime={0.8}>
              <Button
                className='my-6 bg-[#0071E3] px-8 py-4 font-medium text-white'
                radius='full'
              >
                Buy Now
              </Button>
            </Appear>
            <Appear delayTime={1}>
              <p className='text-xl md:text-2xl'>Special’s from $5,200</p>
            </Appear>
          </div>
        </div>
        <Appear delayTime={0.8}>
          <div className='flex h-[150px] justify-center bg-cta-pattern bg-cover bg-center md:h-[300px] lg:h-[399px]'>
            <Image
              src={
                'https://res.cloudinary.com/dysce4sh2/image/upload/v1701066161/s9ux2lbit7gzn08z6jic.png'
              }
              width={1062}
              height={399}
              quality={100}
              alt='bike'
              className='h-auto max-h-[399px] w-full max-w-[300px] md:max-h-full md:max-w-[700px] lg:max-w-[900px] xl:max-w-[1062px]'
            />
          </div>
        </Appear>
      </section>
    </main>
  );
}
