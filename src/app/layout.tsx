import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import './globals.css';
import Providers from '@/components/providers/Providers';
import { neueMontreal } from './fonts';
import Footer from '@/components/layouts/Footer';
import NavbarComponent from '@/components/layouts/Navbar';

export const metadata: Metadata = {
  title: 'Blay',
  description: 'We bring the future of Commuter Bike',
};

const inter = Inter({ subsets: ['latin'] });

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang='en'>
      <body className={`${inter.className} ${neueMontreal.className} `}>
        <Providers>
          <NavbarComponent />
          {children}
          <Footer />
        </Providers>
      </body>
    </html>
  );
}
