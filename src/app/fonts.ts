import localFont from 'next/font/local';

export const neueMontreal = localFont({
  src: [
    {
      path: '../fonts/ppneuemontreal-bold.woff',
      weight: '700',
      style: 'normal',
    },
    {
      path: '../fonts/ppneuemontreal-medium.woff',
      weight: '500',
      style: 'normal',
    },
    {
      path: '../fonts/ppneuemontreal-book.woff',
      weight: '400',
      style: 'normal',
    },
  ],
});
