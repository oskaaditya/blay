import { IconType } from 'react-icons';

export interface LinkProps {
  name: string;
  url: string;
}

export interface LinkMenuProps {
  header: LinkProps;
  submenu: LinkProps[];
}

export interface SocialMediaLinkProps {
  icon: IconType;
  url: string;
}
