export interface BikeProps {
  name: string;
  description: string;
  url: string;
  background: string;
}

interface PartProps {
  name: string;
  description: string;
}

export interface SpecificationListProps {
  name: string;
  part: PartProps[];
}
