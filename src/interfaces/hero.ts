export interface BikeHeroProps {
  color: string;
  url: string;
}
