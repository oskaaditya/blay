export interface StepContent {
  name: string;
  title: string;
  paragraph1: string;
  paragraph2: string;
}
